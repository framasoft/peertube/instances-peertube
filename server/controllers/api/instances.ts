import * as express from 'express'
import { globalStatsValidator } from '../../middlewares/validators/stats.js'
import { About, ServerConfig, ServerStats } from '@peertube/peertube-types'
import { GlobalStats, GlobalStatsHistory, InstanceConnectivityStats, InstanceStatsHistory } from '../../../shared/models/index.js'
import { retryTransactionWrapper } from '../../helpers/database-utils.js'
import { fetchInstance } from '../../helpers/instance-requests.js'
import { logger } from '../../helpers/logger.js'
import { getFormattedObjects } from '../../helpers/utils.js'
import { asyncMiddleware } from '../../middlewares/async.js'
import { setDefaultPagination } from '../../middlewares/pagination.js'
import { setDefaultSort } from '../../middlewares/sort.js'
import { historyListValidator } from '../../middlewares/validators/history.js'
import {
  instanceGetValidator,
  instanceHostsValidator,
  instancesAddValidator,
  instancesListValidator
} from '../../middlewares/validators/instances.js'
import { paginationValidator } from '../../middlewares/validators/pagination.js'
import { instanceHostsSortValidator, instancesSortValidator } from '../../middlewares/validators/sort.js'
import { HistoryModel } from '../../models/history.js'
import { InstanceModel } from '../../models/instance.js'

const instancesRouter = express.Router()

instancesRouter.get('/hosts',
  instancesListValidator,
  instanceHostsValidator,
  paginationValidator,
  instanceHostsSortValidator,
  setDefaultSort,
  setDefaultPagination,
  asyncMiddleware(listInstanceHosts)
)

instancesRouter.get('/',
  instancesListValidator,
  paginationValidator,
  instancesSortValidator,
  setDefaultSort,
  setDefaultPagination,
  asyncMiddleware(listInstances)
)

instancesRouter.get('/:host/stats-history',
  instanceGetValidator,
  asyncMiddleware(getInstanceStatsHistory)
)

instancesRouter.get('/stats',
  globalStatsValidator,
  asyncMiddleware(getGlobalStats)
)

instancesRouter.get('/stats-history',
  historyListValidator,
  asyncMiddleware(getGlobalStatsHistory)
)

instancesRouter.post('/',
  asyncMiddleware(instancesAddValidator),
  asyncMiddleware(createInstanceRetryWrapper)
)

// ---------------------------------------------------------------------------

export {
  instancesRouter
}

// ---------------------------------------------------------------------------

async function createInstanceRetryWrapper (req: express.Request, res: express.Response, next: express.NextFunction) {
  const host = req.body.host

  let config: ServerConfig
  let stats: ServerStats
  let about: About
  let connectivityStats: InstanceConnectivityStats

  try {
    const res = await fetchInstance(host)
    config = res.config
    stats = res.stats
    about = res.about
    connectivityStats = res.connectivityStats
  } catch (err) {
    logger.warn(err)

    return res.status(409)
              .json({
                error: err.message
              })

  }

  const options = {
    arguments: [ { host, config, stats, connectivityStats, about } ],
    errorMessage: 'Cannot insert the instance with many retries.'
  }
  const instance = await retryTransactionWrapper(createInstance, options)

  return res.json({
    instance: {
      id: instance.id,
      host: instance.host
    }
  })
}

async function createInstance (options: {
  host: string
  config: ServerConfig
  stats: ServerStats
  about: About
  connectivityStats: InstanceConnectivityStats
}) {
  const { host, config, stats, about, connectivityStats } = options

  const instanceCreated = await InstanceModel.create({
    host,
    config,
    stats,
    about,
    connectivityStats
  })

  logger.info('Instance %s created.', host)

  return instanceCreated
}

async function listInstances (req: express.Request, res: express.Response) {
  const options = {
    start: req.query.start,
    count: req.query.count,
    sort: req.query.sort,
    signup: req.query.signup,
    healthy: req.query.healthy,
    nsfwPolicy: req.query.nsfwPolicy,
    search: req.query.search,

    // Deprecate *Or
    categoryOneOf: req.query.categoriesOr ?? req.query.categoryOneOf,
    languageOneOf: req.query.languagesOr ?? req.query.languageOneOf,

    liveEnabled: req.query.liveEnabled,
    minUserQuota: req.query.minUserQuota,
    customizations: req.query.customizations,
    filterTag: req.query.filterTag,
    randomSortSeed: req.query.randomSortSeed
  }

  const resultList = await InstanceModel.listForApi(options)

  return res.json(getFormattedObjects(resultList.data, resultList.total))
}

async function listInstanceHosts (req: express.Request, res: express.Response) {
  const options = {
    start: req.query.start,
    count: req.query.count,
    sort: req.query.sort,
    signup: req.query.signup,
    healthy: req.query.healthy,
    nsfwPolicy: req.query.nsfwPolicy,
    search: req.query.search,
    categoriesOr: req.query.categoriesOr,
    languagesOr: req.query.languagesOr,
    minUserQuota: req.query.minUserQuota,
    liveEnabled: req.query.liveEnabled,
    customizations: req.query.customizations,
    since: req.query.since,
    filterTag: req.query.filterTag,
    randomSortSeed: req.query.randomSortSeed
  }

  const resultList = await InstanceModel.listForHostsApi(options)

  return res.json({
    total: resultList.total,
    data: resultList.data.map(d => d.toHostFormattedJSON())
  })
}

async function getGlobalStats (req: express.Request, res: express.Response) {
  const data: GlobalStats = await InstanceModel.getStats({ includeAll: req.query.includeAll })

  return res.json(data)
}

async function getInstanceStatsHistory (req: express.Request, res: express.Response) {
  const instance = res.locals.instance

  const rows = await HistoryModel.getInstanceHistory(instance.id)

  const result: InstanceStatsHistory = {
    data: rows.map(d => d.toFormattedJSON())
  }

  return res.json(result)
}

async function getGlobalStatsHistory (req: express.Request, res: express.Response) {
  const beforeDate = req.query.beforeDate
  const includeAll = req.query.includeAll

  const rows = await HistoryModel.getGlobalStatsHistory({ beforeDate, includeAll })

  const result: GlobalStatsHistory = {
    data: rows
  }

  return res.json(result)
}
