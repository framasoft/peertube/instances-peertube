import express from 'express'
import { badRequest } from '../../helpers/utils.js'
import { instancesRouter } from './instances.js'
import { configRouter } from './config.js'
import { adminRouter } from './admin.js'

const apiRouter = express.Router()

apiRouter.use('/', configRouter)
apiRouter.use('/instances', instancesRouter)
apiRouter.use('/admin', adminRouter)
apiRouter.use('/*', badRequest)

// ---------------------------------------------------------------------------

export { apiRouter }
