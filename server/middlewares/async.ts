import Bluebird from 'bluebird'
import { NextFunction, Request, RequestHandler, Response } from 'express'
import { ValidationChain } from 'express-validator'

export type RequestPromiseHandler = ValidationChain | ((req: Request, res: Response, next: NextFunction) => Promise<any>)

function asyncMiddleware (fun: RequestPromiseHandler | RequestPromiseHandler[]) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (Array.isArray(fun) === true) {
      return Bluebird.each(fun, f => {
        return new Promise<void>((resolve, reject) => {
          return asyncMiddleware(f)(req, res, err => {
            if (err) return reject(err)

            return resolve()
          })
        })
      }).then(() => next())
        .catch(err => next(err))
    }

    return Promise.resolve((fun as RequestHandler)(req, res, next))
      .catch(err => next(err))
  }
}

// ---------------------------------------------------------------------------

export {
  asyncMiddleware
}
