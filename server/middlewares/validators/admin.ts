// Create express middleware to check Bearer token using express validator
import express from 'express'
import { body } from 'express-validator'
import { isHostValid } from '../../helpers/custom-validators/instances.js'
import { logger } from '../../helpers/logger.js'
import { CONFIG } from '../../initializers/constants.js'
import { InstanceModel } from '../../models/instance.js'
import { areValidationErrors } from './utils.js'

export const checkAdminTokenValidator = [
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const authorization = req.headers['authorization']

    if (!req.headers['authorization']) {
      return res.status(401).json({ error: `An admin token must be specified` })
    }

    if (!CONFIG.ADMIN_API_KEY) {
      return res.status(400).json({ error: `Missing admin API key in config` })
    }

    if (authorization.replace(/Bearer /, '') !== CONFIG.ADMIN_API_KEY) {
      return res.status(403).json({ error: `Authorization header is invalid` })
    }

    return next()
  }
]

export const adminSetFilterTagsValidator = [
  body('filterTags').isArray().withMessage('Should have a valid filterTags array'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.body }, 'Checking admin set filter tags parameters')

    if (areValidationErrors(req, res)) return

    return next()
  }
]

export const adminGetInstanceByHostValidator = [
  body('host').custom(isHostValid).withMessage('Should have a valid host'),

  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const instance = await InstanceModel.loadByHost(req.body.host)
    if (!instance) {
      return res.status(404).json({ error: `Instance not found` })
    }

    res.locals.instance = instance

    return next()
  }
]
