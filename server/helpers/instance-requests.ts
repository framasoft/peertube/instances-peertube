import { About, ServerConfig, ServerStats } from '@peertube/peertube-types'
import { InstanceConnectivityStats } from '../../shared/models/index.js'
import { countryLookup } from '../initializers/geoip.js'
import { isTestInstance } from './core-utils.js'
import { doJSONRequest } from './requests.js'
import { faultTolerantResolve } from './utils.js'

async function fetchInstanceConfig (host: string) {
  const path = '/api/v1/config'
  const url = getScheme() + host + path

  const { body } = await doJSONRequest<ServerConfig>(url)

  return body
}

async function fetchInstanceAbout (host: string) {
  const path = '/api/v1/config/about'
  const url = getScheme() + host + path

  const { body } = await doJSONRequest<About>(url)

  return body
}

async function fetchInstanceStats (host: string) {
  const path = '/api/v1/server/stats'
  const url = getScheme() + host + path

  const { body } = await doJSONRequest<ServerStats>(url)

  const ipv4Addresses = await faultTolerantResolve(host, 'A')
  const ipv6Addresses = await faultTolerantResolve(host, 'AAAA')
  const addresses = [ ...ipv4Addresses, ...ipv6Addresses ]

  const supportsIPv6 = ipv6Addresses.length > 0
  const country = (addresses.length > 0)
    ? (await countryLookup(addresses[0])).country?.iso_code // lookup does a country lookup on a local geoip db downloaded on first use
    : undefined

  return {
    stats: body,
    connectivityStats: {
      supportsIPv6,
      country
    } as InstanceConnectivityStats
  }
}

async function fetchInstance (host: string) {
  const config = await fetchInstanceConfig(host)
  const about = await fetchInstanceAbout(host)
  const { stats, connectivityStats } = await fetchInstanceStats(host)

  if (!config || !stats || config.serverVersion === undefined || stats.totalVideos === undefined) {
    throw new Error('Invalid remote host. Are you sure this is a PeerTube instance?')
  }

  return { config, stats, about, connectivityStats }
}

// ---------------------------------------------------------------------------

export {
  fetchInstance,
  fetchInstanceConfig,
  fetchInstanceStats,
  fetchInstanceAbout
}

// ---------------------------------------------------------------------------

function getScheme () {
  if (isTestInstance()) return 'http://'

  return 'https://'
}
