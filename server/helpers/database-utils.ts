import pRetry from 'p-retry'
import { logger } from './logger.js'

type RetryTransactionWrapperOptions = { errorMessage: string, arguments?: any[] }

function retryTransactionWrapper <T> (fun: (...args: any[]) => Promise<T>, options: RetryTransactionWrapperOptions): Promise<T> {
  return transactionRetryer<T>(fun, options.arguments)
    .catch(err => {
      console.error(err)
      logger.error({ err }, options.errorMessage)
      throw err
    })
}

function transactionRetryer <T> (func: (...args: any[]) => Promise<T>, args: any[] = []) {
  return pRetry(() => func(...args), {
    retries: 5,

    shouldRetry: err => {
      const willRetry = (err.name === 'SequelizeDatabaseError')
      logger.debug({ willRetry }, 'Maybe retrying the transaction function.')
      return willRetry
    }
  })
}

// ---------------------------------------------------------------------------

export {
  retryTransactionWrapper,
  transactionRetryer
}
