import { NSFWPolicyType } from '@peertube/peertube-types'
import { InstanceStats } from './instance-stats.model.js'

interface InstanceImage {
  width: number
  url: string
}

export interface Instance extends InstanceStats {
  id: number
  host: string

  name: string
  shortDescription: string
  version: string

  signupAllowed: boolean
  signupRequiresApproval: boolean

  userVideoQuota: number

  liveEnabled: boolean

  categories: number[]
  languages: string[]

  autoBlacklistUserVideosEnabled: boolean
  defaultNSFWPolicy: NSFWPolicyType
  isNSFW: boolean

  avatars: InstanceImage[]
  banners: InstanceImage[]

  supportsIPv6?: boolean
  country?: string

  health: number

  createdAt: string

  // More stats
  totalModerators: number
  totalAdmins: number

  averageRegistrationRequestResponseTimeMs: number
  totalRegistrationRequestsProcessed: number
  totalRegistrationRequests: number

  averageAbuseResponseTimeMs: number
  totalAbusesProcessed: number
  totalAbuses: number
}
