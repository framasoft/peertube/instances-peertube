import 'bootstrap/js/dist/dropdown.js'
import 'bootstrap/js/dist/collapse.js'
import './sass/bootstrap.scss'
import { createApp } from 'vue'
import CountryFlag from 'vue-country-flag-next'
// FIXME: bad typings for ESM import and tree shaking
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { Table } from '@oruga-ui/oruga-next'
import App from './App.vue'
import router from './router.js'

const app = createApp(App)

app.component('CountryFlag', CountryFlag)

app.use(Table)

app.use(router)
app.mount('#app')

